import { productsTypes } from "../types";

const initialState = {
  favoriteProducts:
    JSON.parse(localStorage.getItem("Favorite")) ||
    localStorage.setItem("Favorite", JSON.stringify([])),
};

export function favoriteProductsReducer(state = initialState, action) {
  switch (action.type) {
    case productsTypes.SET_FAVORITE_PRODUCTS:
      return {
        ...state,
        favoriteProducts: action.payload,
      };

    default:
      return state;
  }
}
