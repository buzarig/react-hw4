import { productsTypes } from "../types";

const initialState = {
  cartProducts:
    JSON.parse(localStorage.getItem("Cart")) ||
    localStorage.setItem("Cart", JSON.stringify([])),
};

export function cartProductsReducer(state = initialState, action) {
  switch (action.type) {
    case productsTypes.SET_CART_PRODUCTS:
      return {
        ...state,
        cartProducts: action.payload,
      };

    default:
      return state;
  }
}
