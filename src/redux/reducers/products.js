import { productsTypes } from "../types";

const initialState = {
  currentProducts: [],
};

export function productsReducer(state = initialState, action) {
  switch (action.type) {
    case productsTypes.FETCH_PRODUCTS_SUCCESS:
      return {
        currentProducts: action.payload,
      };

    default:
      return state;
  }
}
