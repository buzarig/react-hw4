import { productsTypes } from "../types";

export const setCartProducts = (cartProducts) => ({
  type: productsTypes.SET_CART_PRODUCTS,
  payload: cartProducts,
});
