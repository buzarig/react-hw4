import Products from "../components/products/Products";
import React, { useEffect } from "react";
import Modal from "../components/modal/Modal";
import Cart from "../components/cart/Cart";
import Favorite from "../components/favorite/Favorite";
import { useDispatch, useSelector } from "react-redux";
import { openModal, closeModal } from "../redux/actions/modal";
import { fetchProducts } from "../redux/actions/products";
import { setCartProducts } from "../redux/actions/cartProducts";
import { setFavoriteProducts } from "../redux/actions/favoriteProducts";

export function Home() {
  const dispatch = useDispatch();
  const modal = useSelector((state) => state.modal);
  const products = useSelector((state) => state.products.currentProducts);
  const favoriteProducts = useSelector(
    (state) => state.favorite.favoriteProducts
  );
  const cartProducts = useSelector((state) => state.cart.cartProducts);

  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  // const [cartProducts, setCartProducts] = useState(
  //   JSON.parse(localStorage.getItem("Cart")) ||
  //     localStorage.setItem("Cart", JSON.stringify([]))
  // );
  // const [favoriteProducts, setFavoriteProducts] = useState(
  //   JSON.parse(localStorage.getItem("Favorite")) ||
  //     localStorage.setItem("Favorite", JSON.stringify([]))
  // );

  const updateCart = () => {
    const updateCartArray = JSON.parse(localStorage.getItem("Cart"));
    dispatch(setCartProducts(updateCartArray));
  };

  const updateFavorite = () => {
    const updateFavoriteArray = JSON.parse(localStorage.getItem("Favorite"));
    dispatch(setFavoriteProducts(updateFavoriteArray));
  };

  const handleOpenModal = (modalId, modalSubmit) => {
    dispatch(openModal(modalId, modalSubmit));
  };

  const handleCloseModal = () => {
    dispatch(closeModal());
  };

  return (
    <>
      <header className="header">
        <Favorite counter={favoriteProducts.length} />
        <Cart counter={cartProducts.length} />
      </header>
      <Products
        products={products}
        favoriteProducts={favoriteProducts}
        cartProducts={cartProducts}
        openModal={handleOpenModal}
        updateFavorite={updateFavorite}
        updateCart={updateCart}
      />
      {modal.showModal && (
        <Modal
          close={handleCloseModal}
          modalId={modal.modalId}
          submit={modal.modalSubmit}
        />
      )}
    </>
  );
}
