import React from "react";
import styles from "./Favorite.module.scss";
import { Icon } from "@iconify/react";

const Favorite = ({ counter }) => {
  return (
    <div className={styles.Container}>
      <span className={styles.Count}>{counter}</span>
      <Icon icon="bi:star" width="50" height="50" color="white" />
    </div>
  );
};

export default Favorite;
