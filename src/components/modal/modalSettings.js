const modalWindowDeclarations = [
  {
    modalId: "toCartModal",
    modalProps: {
      title: "Підтвердження",
      description: "Чи згодні ви додати цей товар у кошик?",
      background: "white",
      closeButton: true,
      actions: [
        {
          text: "Ні",
          background: "red",
          type: "cancel",
        },
        {
          text: "Так",
          background: "green",
          type: "submit",
        },
      ],
    },
  },
  {
    modalId: "removeFromCartModal",
    modalProps: {
      title: "Підтвердження",
      description: "Чи хочете ви видалити цей товар з кошику?",
      background: "rgb(80, 2, 2)",
      closeButton: true,
      actions: [
        {
          text: "No",
          background: "red",
          type: "cancel",
        },
        {
          text: "Yes",
          background: "green",
          type: "submit",
        },
      ],
    },
  },
];

export default modalWindowDeclarations;
